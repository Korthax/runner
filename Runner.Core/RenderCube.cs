﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Runner.Core
{
    public class RenderCube
    {
        private static readonly Vector3[] PositionVertexBufferData = {
            new Vector3(-1.0f, -1.0f,  1.0f),
            new Vector3( 1.0f, -1.0f,  1.0f),
            new Vector3( 1.0f,  1.0f,  1.0f),
            new Vector3(-1.0f,  1.0f,  1.0f),
            new Vector3(-1.0f, -1.0f, -1.0f),
            new Vector3( 1.0f, -1.0f, -1.0f), 
            new Vector3( 1.0f,  1.0f, -1.0f),
            new Vector3(-1.0f,  1.0f, -1.0f) 
        };

        private static readonly int[] IndexBufferData = {
            // front face
            0, 1, 2, 2, 3, 0,
            // top face
            3, 2, 6, 6, 7, 3,
            // back face
            7, 6, 5, 5, 4, 7,
            // left face
            4, 0, 3, 3, 7, 4,
            // bottom face
            0, 1, 5, 5, 4, 0,
            // right face
            1, 5, 6, 6, 2, 1
        };

        private readonly int _vertexArrayHandle;

        public static RenderCube Create()
        {
            var positionHandle = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, positionHandle);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(PositionVertexBufferData.Length * Vector3.SizeInBytes), PositionVertexBufferData, BufferUsageHint.StaticDraw);

            var normalHandle = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, normalHandle);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(PositionVertexBufferData.Length * Vector3.SizeInBytes), PositionVertexBufferData, BufferUsageHint.StaticDraw);

            var elementHandle = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, elementHandle);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(sizeof(uint) * IndexBufferData.Length), IndexBufferData, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

            var vertexArrayHandle = GL.GenVertexArray();
            GL.BindVertexArray(vertexArrayHandle);

            GL.EnableVertexAttribArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, positionHandle);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, Vector3.SizeInBytes, 0);

            GL.EnableVertexAttribArray(1);
            GL.BindBuffer(BufferTarget.ArrayBuffer, normalHandle);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, true, Vector3.SizeInBytes, 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, elementHandle);

            GL.BindVertexArray(0);

            return new RenderCube(vertexArrayHandle);
        }

        private RenderCube(int vertexArrayHandle)
        {
            _vertexArrayHandle = vertexArrayHandle;
        }

        public void Render()
        {
            GL.BindVertexArray(_vertexArrayHandle);
            GL.DrawElements(PrimitiveType.Triangles, IndexBufferData.Length, DrawElementsType.UnsignedInt, IntPtr.Zero);
        }
    }
}