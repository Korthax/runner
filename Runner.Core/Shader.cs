﻿using System;
using System.IO;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Runner.Core
{
    public interface IInactiveShader : IDisposable
    {
        IActiveShader Use();
    }

    public interface IActiveShader : IDisposable
    {
        IActiveShader BindMatrix4(string parameter, Matrix4 value);
    }
    
    public class Shader : IInactiveShader, IActiveShader
    {
        private readonly int _shaderProgramHandle;

        public static IInactiveShader Load(string filePath)
        {

            var vertexShaderHandle = GL.CreateShader(ShaderType.VertexShader);
            GL.ShaderSource(vertexShaderHandle, File.ReadAllText($"{filePath}/vs.glsl"));
            GL.CompileShader(vertexShaderHandle);
            Console.WriteLine(GL.GetShaderInfoLog(vertexShaderHandle));

            var fragmentShaderHandle = GL.CreateShader(ShaderType.FragmentShader);
            GL.ShaderSource(fragmentShaderHandle, File.ReadAllText($"{filePath}/fs.glsl"));
            GL.CompileShader(fragmentShaderHandle);
            Console.WriteLine(GL.GetShaderInfoLog(fragmentShaderHandle));

            var shaderProgramHandle = GL.CreateProgram();
            GL.AttachShader(shaderProgramHandle, vertexShaderHandle);
            GL.AttachShader(shaderProgramHandle, fragmentShaderHandle);
            
            GL.BindAttribLocation(shaderProgramHandle, 0, "in_position");
            GL.BindAttribLocation(shaderProgramHandle, 1, "in_normal");

            GL.LinkProgram(shaderProgramHandle);
            Console.WriteLine(GL.GetProgramInfoLog(shaderProgramHandle));

            GL.DeleteShader(vertexShaderHandle);
            GL.DeleteShader(fragmentShaderHandle);
            
            return new Shader(shaderProgramHandle);
        }

        private Shader(int shaderProgramHandle)
        {
            _shaderProgramHandle = shaderProgramHandle;
        }

        public IActiveShader Use()
        {
            GL.UseProgram(_shaderProgramHandle);
            return this;
        }

        public IActiveShader BindMatrix4(string name, Matrix4 matrix)
        {
            var uniformLocation = GL.GetUniformLocation(_shaderProgramHandle, name);
            GL.UniformMatrix4(uniformLocation, false, ref matrix);

            return this;
        }

        public IActiveShader Bind(ref Matrix4 viewMatrix, ref Matrix4 projectionMatrix)
        {
            var projectionMatrixLocation = GL.GetUniformLocation(_shaderProgramHandle, "projection_matrix");
            GL.UniformMatrix4(projectionMatrixLocation, false, ref projectionMatrix);

            var modelviewMatrixLocation = GL.GetUniformLocation(_shaderProgramHandle, "modelview_matrix");
            GL.UniformMatrix4(modelviewMatrixLocation, false, ref viewMatrix);

            return this;
        }

        public void Dispose()
        {
            GL.ReleaseShaderCompiler();
        }
    }
}