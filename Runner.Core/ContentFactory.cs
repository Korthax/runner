﻿using System;
using System.Collections.Concurrent;
using Serilog;

namespace Runner.Core
{
    public class ContentFactory : IDisposable
    {
        private readonly ConcurrentDictionary<string, Texture2D> _textures;
        private readonly string _contentRoot;
        private readonly ILogger _logger;

        public ContentFactory(string contentRoot, ILogger logger)
        {
            _contentRoot = contentRoot;
            _logger = logger;
            _textures = new ConcurrentDictionary<string, Texture2D>();
        }
        
        public void Dispose()
        {
            foreach (var (name, texture) in _textures)
            {
                _logger.Information("Releasing texture '{name}'.", name);
                texture.Dispose(); 
            }
        }

        public Texture2D Load(string name)
        {
            Texture2D result;
            if (_textures.TryGetValue(name, out result)) 
                return result;
            
            result = new Texture2D(ImageSharp.Image.Load($"{_contentRoot}/{name}.jpg"));
            _textures.TryAdd(name, result);
            return result;
        }
    }
}