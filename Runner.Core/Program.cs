﻿using System;
using System.Collections.Generic;
using Autofac;
using Serilog;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;


namespace Runner.Core
{
    public class Map
    {
        private readonly RenderCube _renderCube;
        private readonly Shader _shader;
        private int[,] _data;

        public static Map From(Size size, Size offset, Shader shader)
        {
            return new Map(shader, RenderCube.Create(), new int[size.Width, size.Height], offset);
        }
        
        public Map(Shader shader, RenderCube renderCube, int[,] data, Size offset)
        {
            _shader = shader;
            _renderCube = renderCube;
            _data = data;
        }
        
        public void Render()
        {
            for (var y = 0; y < _data.GetUpperBound(1); y++)
            {
                for (var x = 0; x < _data.GetUpperBound(0); x++)
                {
                    _renderCube.Render();
                }
            }
        }
    }

    public class Game : GameWindow
    {
        private Matrix4 _projectionMatrix;
        private Matrix4 _modelviewMatrix;
        private RenderCube _renderCube;
        private IActiveShader _shader;

        public Game()
            : base(640, 480,
                new GraphicsMode(), "OpenGL 3 Example", 0,
                DisplayDevice.Default, 3, 2,
                GraphicsContextFlags.ForwardCompatible | GraphicsContextFlags.Debug)
        {
        }

        protected override void Dispose(bool manual)
        {
            _shader.Dispose();
            base.Dispose(manual);
        }

        protected override void OnLoad(EventArgs eventArgs)
        {
            VSync = VSyncMode.On;

            _renderCube = RenderCube.Create();
            
            var aspectRatio = ClientSize.Width / (float)ClientSize.Height;
            Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, aspectRatio, 1, 100, out _projectionMatrix);
            _modelviewMatrix = Matrix4.LookAt(new Vector3(0, 3, 5), new Vector3(0, 0, 0), new Vector3(0, 1, 0));

            _shader = Shader
                .Load("content/cube")
                .Use()
                .BindMatrix4("modelview_matrix", _modelviewMatrix)
                .BindMatrix4("projection_matrix", _projectionMatrix);

            GL.Enable(EnableCap.DepthTest);
            GL.ClearColor(Color.MidnightBlue);
        }

        protected override void OnUpdateFrame(FrameEventArgs eventArgs)
        {
            var keyboard = Keyboard.GetState();
            if (keyboard[Key.Escape])
                Exit();
        }

        protected override void OnRenderFrame(FrameEventArgs eventArgs)
        {
            GL.Viewport(0, 0, Width, Height);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            _renderCube.Render();
            SwapBuffers();
        }
        
        [STAThread]
        public static void Main()
        {
            var logger = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();

            var containerBuilder = new ContainerBuilder();

            containerBuilder
                .RegisterType<Game>()
                .AsSelf()
                .SingleInstance();

            containerBuilder
                .RegisterType<ContentFactory>()
                .AsSelf()
                .SingleInstance();

            containerBuilder
                .RegisterInstance(logger)
                .As<ILogger>()
                .SingleInstance();

            var container = containerBuilder
                .Build();

            var lifetimeScope = container.BeginLifetimeScope();
            using (var game = lifetimeScope.Resolve<Game>())
                game.Run(30);
        }
    }
}